# Python setup: Conda+VScode+Jupyter
There are several ways to install python in your computer:
- Download the installer package in :[Python official Download page](https://www.python.org/downloads/)
- Using `apt-get install` or `yum install` in Linux distributions like Ubuntu and CentOS, or `brew install` in MacOS
- Using conde: [conda official website](https://docs.conda.io/en/latest/)

Here is the reason why I recommand using conda install and manage python environments and packages, which I quote from [conda official website](https://docs.conda.io/en/latest/)

> Conda is an open source package management system and environment management system that runs on Windows, macOS, Linux and z/OS. Conda quickly installs, runs and updates packages and their dependencies. Conda easily creates, saves, loads and switches between environments on your local computer. It was created for Python programs, but it can package and distribute software for any language.
> Conda as a package manager helps you find and install packages. If you need a package that requires a different version of Python, you do not need to switch to a different environment manager, because conda is also an environment manager. With just a few commands, you can set up a totally separate environment to run that different version of Python, while continuing to run your usual version of Python in your normal environment.

From above, we can see conda can:
- cross platform: Windows, MacOS and Linux: easily install and move
- manage python packages very effectively: easily install packages
- switch different python environment: debug and change among different workspace

Besides, conda is included in [TUNA](https://mirrors.tuna.tsinghua.edu.cn/), so we chan install packages and softwares in severs that not open to "outside internet" using Conda

## Install Conda
We can install conda from the [offical site](https://docs.conda.io/projects/conda/en/latest/) or [TUNA](https://mirrors.tuna.tsinghua.edu.cn/), here I will show the process based on [TUNA](https://mirrors.tuna.tsinghua.edu.cn/)
### Anaconda and Miniconda
First of all, there are two option for conda installation: `anaconda` and `miniconda`, eachwill contain fullly functional `conda` but:
- `anaconda` has lots of python packages build inside and also have a GUI window installed;
- `miniconda` only has basic `conda` and `python` installed, and no GUI window, which means we can use miniconda only in terminals.

Personally, I recommand `miniconda` because (1) I am familiar with terminal operation (2) it sometime will be package confict in `anaconda` because of too much packages, among which only several packages are usful for my daily work. But `anaconda` will be friendly if you are not familiar with terminal operation as a beginner.

We can found `conda` installer in [Anaconda 镜像使用帮助](https://mirrors.tuna.tsinghua.edu.cn/help/anaconda/), and download page of [anaconda](https://mirrors.tuna.tsinghua.edu.cn/anaconda/archive/) and [miniconda](https://mirrors.tuna.tsinghua.edu.cn/anaconda/miniconda/)

### Installation of Conda
1. select the right installer based on different platform different, like `Miniconda3-py39_4.12.0-Windows-x86_64.exe` here `py39_4.12.0` is the initial python version in this installer, `Windows` is the computer system, and `x84_64` is the cpu architecture (64 bits x86), `exe` is the file type of this installer. so `Miniconda3-py39_4.12.0-Windows-x86_64.exe` means is installer contains a python3.9 environment, and should be used in a Windows system with an Intel or AMD 64 bits cpu.

2. run the installer, for `exe` and `pkg`, we can open them and install, 

3. for `.sh` file we should run `sh installer_name.sh` in terminal like: `sh Miniconda3-py39_4.12.0-Linux-x86_64.sh`

### Basic usage of conda
A python environment will be installed together with conda, so we can install python packages and creat different environments using conda.

Here is some example using terminal to do conda operation, my terminal emulator is `kitty` and shell is `zsh`
1. check python environment: 
    - `python --version`: check python version
    - `which python`: check python path

![conda_1](./python_env_setup/conda_1.gif)

2. install packages: example `pandas` and `cython`
    - `conda install pandas`: install `pandas` from conda default `main` channel
    - `conda install cython -c conda-forge`: install `cython` from the a `conda-forge` ; a community channel

![conda_2](./python_env_setup/conda_2.gif)

- Here: different channel means different source we can find the package, and packages `cython` is not included in `main` channel and inclued in `conda-forge`. To found aout more information about a package (like which channel and which platform we can use), we can search the package name in this [website](https://anaconda.org/anaconda/repo)

3. uninstall packages:
    - `conda uninstall pandas cython`: uninstall pandas and cython at the same time, adn we can also install several pacakges at the same time by running `conda install pkg_1 pkg_2 pkg3`

![conda_3](./python_env_setup/conda_3.gif)

4. create and switch among different environments:
    - `conda create -n python_env_test_1 python=3.8`: create a new conda environment named `python_evn_test_1` and `python` version is `3.8`
    - `conda env list`: show all conda environments, in which the `*env_name` is the environment you currently in;
    - `conda activate python_evn_test_1`: switch into the conda environment named `python_evn_test_1`;

![conda_4](./python_env_setup/conda_4.gif)

5. remove environment:
    - `conda deactivate`: switch back to the `base` environment
    - `conda remove -n python_evn_test_1 --all`: remove the `python_evn_test_1` environment;

![conda_5](./python_env_setup/conda_5.gif)

6. other options:
    - `conda --help`: check the help of conda;
    - `conda search`: search a package;
    - `conda config`: show or make some change of the conda configuration
    - `conda update`: update pacakges

## VScode(Visual Studio Code)
VScode is a lightweight but powerful source code editor, it is open source, cross platform and has a rich ecosystem of extension for othter language and runtimes (such as C, C++, Python, Latex, R .etc). There are lots of introductions like videos or blogs you can start with, just search `VScode` in **Baidu** or **Google**

### Download and installation
Here is the [offical page of VScode](https://code.visualstudio.com/), just click and select the right installer fit your platform (arm or x86 in Windows MarOS or Linux)
- I tried to install `VScode` in **Arch**, and the best option is using `yay -S code-insiders`
- In Mac, be causeful about the `M1(arm)` version and `intel(x86)` version

Here I won't introduce two much about "how to use code to write and run code", just show you several very useful extensions for daily usage

![vscode_1](./python_env_setup/vscode-1.png)

- Python (Include Jupyter extension)
- PathAutocomplete (Autocomplete the path when we start type "./" or "/")
- Better Git Tools (Git History and Git Graph)
- Different color themes (makes code looks clear)
- LatexWorkshop (Best Latex support)
- vscode-pdf (view pdf in VScode)
- Remote-SSH (connect to remote server in VScode: view and edit)
- Remote-WSL (only in windows, connect to WSL)
- IntelliCode (AI feature for python development)

Here is a reference in youtube: [Visual Studio Code 2022 | Web Dev Setup | Top Extensions, Themes, Settings, Tips & Tricks](https://www.youtube.com/watch?v=fJEbVCrEMSE)
![Visual Studio Code 2022 | Web Dev Setup | Top Extensions, Themes, Settings, Tips & Tricks](./Conda+VScode+Jupyter/VScode_refere.mp4)

### Demo of Writing Python in using vscode
![vscode_demo](./python_env_setup/vscode_demo.mp4)
1. ceate a file named `demo.py`
2. write code in `demo.py`
    - import a package named "os"
    - define a string variable "path", and value it as a path (PathAutocomplete will show the suggestion when type './' or '../')
    - print all files and folders under "../J_group"
    - change the python interpreter from `"/Users/frank/miniconda3/envs/ipycles/bin/python"` to `"/Users/frank/miniconda3/bin/python"`

## Jupyter and use Jupyter in VScode
Jupyter is a package name from JupyterLab, which provide a flexible interface allows users to configure and arrange workflows in data science, scientific computing, computational journalism, etc. To use Jupyter we have install it in conda by

`conda install jupyter`

Then run `jupyter notebook` in terminal and it will use the default browser create a JupyterLab interface.

VScode also has a great support of Jupyter, we can use those extensions to write and run python in the `.ipynb` files

To do that, we have to make sure:
1. jupyter package is installed by conda (or pip) under current environment;
2. VScode extention named `Python` and `Jupyter` is also installed

![vscode_jupyter](./python_env_setup/vscode-jupyter.png)

Here is a demo of using VScode write Jupyternote file, you can open it with VScode and play with it: [Jupyter_demo](./python_env_setup/jupyter_demo/demo_jupyter.ipynb)

Keypoints:
- run the current cell (where the cursor is on): `ctrl+Inter`
- view of vscode+jupyter can be rearranged, like:

![vscode_demo](./python_env_setup/vscode_demo.png)

