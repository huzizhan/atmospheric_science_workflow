# atmospheric_science_workflow

This project aims to organize and introduce powerful tools in my daily workflow as a Ph.D student of Atmospheric Science in Tsinghua University.
这个项目计划整理一些我在清华读大气科学博士期间用到很牛的软件和工具，以及日常的工作流程

In general, I using python as the main tool to deal with data analysis and plotting in MacOS and Linux(also include Windows Subsystem Linux: WSL), so this project is mainly about setups and tools about python in UNIX like system.
总的而言，我日常主要使用python在MacOS和Linux系统（包括Windows 的 WSL）下来处理数据分析和画图，所以整个项目的主要内容是一些针对python的在类UNIX系统下的环境配置和适用工具

## Outline
- Python workflow: [Conda+VScode+Jupyter](./Conda+VScode+Jupyter.md)
- Windows setup: [Win11+WSL+VScode](./Win11+WSL+VScode.md)
## LICENSE
MIT
