## 中文输入问题
1. Linux程序配置中文字体：
	- 参考ctex宏包具体教程：[CTeX 手册](https://mirror-hk.koddos.net/CTAN/language/chinese/ctex/ctex.pdf)
	- 安装相应的字体，下载连接参考：[wFonts.com](https://www.wfonts.com)
	- 使用特定的字体，参考xeCJK宏包使用手册：[xeCJK](http://mirrors.ibiblio.org/CTAN/macros/xetex/latex/xecjk/xeCJK.pdf)
2. 参考帖（中文）：[全面总结如何在 LaTeX 中使用中文 (2020 最新版)](https://jdhao.github.io/2018/03/29/latex-chinese.zh/)
