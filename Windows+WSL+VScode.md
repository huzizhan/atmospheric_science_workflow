# Windows+WSL+VScode setup
This introduction is based on up-to-date Windows System (both win10 and win11 with last version has been tested).

## What is WSL
WSL: Windows Subsystem Linux, which means we can run linux programs in Windows system without a virtual machine installed (using tools like VMare Workstation). 
In most cases, we will use WSL only in terminal (we can use some interactive GUI software in WSL but it takes time to figure out and make configuration, so personally I don't recommand). 
Fortunately, VScode has a very good support on WSL, so we can run Linux programs and scripts in VScode+WSL juset like in a fully functional Linux system.

**Microsoft** has a very intergrated website introduce **How to install WSL in a windows computer**: [WSL installation](https://docs.microsoft.com/zh-cn/windows/wsl/install). 

More information about WSL are included in here: [documents about WSL](https://docs.microsoft.com/zh-cn/windows/wsl/installc).

Here a guide video about installation and basic usage of WSL:
1. [(youtube) install WSL2 in windows 10](https://youtu.be/n-J9438Mv-s)
2. [(youtube) install WSL in windows 11](https://youtu.be/FQ6ahcJOVz0)
3. [(youtube) WSL get started](https://youtu.be/_fntjriRe48)

## Demo shows How to install WSL2 in Windows10
This section will show the process about install WSL in Windows10, which may contains some problems that most peopel will meet. 

1. The system version is 2H21 (19043)
2. Open the panel of "Turn Windows features on or off (启动或关闭Windows功能)", and enable "Linux Subsystem (适用于Linux的Windows子系统)" and "Virtual Machine Platform (虚拟机平台)":
![WSL-0](./WSL/wsl-0-windows_feature.gif)
    finish the process and reboot.

3. Another option is follow this link: [旧版 WSL 的手动安装步骤](https://docs.microsoft.com/zh-cn/windows/wsl/install-manual#step-3---enable-virtual-machine-feature), and run following commands in **Poweshell**, as Administraton
    ```
    dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
    ```
    and
    ```
    dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
    ```
    ![WSL-0.5](./WSL/wsl-0-command-open-feature.gif)

4. **Upgrade WSL-1 to WSL-2**: Run 'wsl -l -v' to check the version of WSL. From now on, I use a ternimal software called "**Windows Terminal （终端）**" to run all commands related with WSL:
    - Check the link here for updating WSL version: [旧版 WSL 的手动安装步骤](https://docs.microsoft.com/zh-cn/windows/wsl/install-manual#step-3---enable-virtual-machine-feature)
    - Download the updating package here and open it: [适用于 x64 计算机的 WSL2 Linux 内核更新包](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi)
    - set the default WSL version as 2, then check the version
    ```
    wsl --set-default-version 2
    ```
    If you want to install WSL1, skip this section.

5. Follow the documentation above: [WSL installation](https://docs.microsoft.com/zh-cn/windows/wsl/install), open poweshell and type `wsl` to see wether there is `wsl` tools and `wsl --list -o` to see which WSL distribution we can install:
![WSL-1](./WSL/wsl-1.gif)

6. type 'wsl --install -d Ubuntu-20.04' to install `Ubuntu 20.04 LTS`(a Ubuntu WSL distribution). That will takes several minutes to finish, and the `Ubuntu-20.04` will be launched automatically after the installation.
![WSL_install](./WSL/wsl_install.gif)

7. Input the `name` and `password` in the `Ubuntu-20.04` and enjoy your journey with WSL

8. Another place to install WSL distributions, is the Microsoft AppStore. You can search for "WSL" or the name of WSL distribution and install the correct result. Here shows an example as "install AlmaLinux-9", which is using the same kernel as Cenots and I personally recommand it very much.
![wsl_alma](./WSL/install-alma.gif)

9. Some tricks of using WSL:
    - Open the WSL by click the software icon; 
    - Use "Windows Terminal （终端）" and select the WSL distribution you want to work with.
![wsl_usage](./WSL/wsl_usage.gif)

## Use WSL in VSCode
There is a very good support of WSL in vscode, by using an extension called **[Remote-WSL](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-wsl)**

- The installation of **Remote-WSL** in VSCode is very simple:
![wsl_vscode](./WSL/wsl_vscode.gif)

- And we can also open current *Windows Folder* in WSL using VSCode, just like
:
![wsl_vscode_reopen](./WSL/wsl_vscode_reopen_wsl.gif)

- We can also open a terminal window in VSCode, which is vey useful for running Python scripts or dealing with climate data using [cdo](https://code.mpimet.mpg.de/projects/cdo/wiki/Tutorial#:~:text=The%20Climate%20Data%20Operators%20(CDO)%20software%20is%20a,data%20selection%20and%20subsampling%20tools%2C%20and%20spatial%20interpolation.) in WSL.
![wsl_vscode_terminal](./WSL/wsl_vscode_terminal.gif)
